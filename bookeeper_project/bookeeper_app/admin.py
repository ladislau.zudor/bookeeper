from django.contrib import admin
from bookeeper_app.models import *


# Register your models here.

admin.site.register(Category)
admin.site.register(Room)
admin.site.register(Shelf)
admin.site.register(Author)
admin.site.register(Publisher)
admin.site.register(Book)