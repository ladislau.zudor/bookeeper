from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect, render
from django.views.generic import DetailView, UpdateView
from django.contrib.auth.decorators import login_required
from django.urls import reverse, reverse_lazy
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash

from .forms import *
from .models import *


################  ----- View for homepage display, with user login and processing of data entered by the user --------------
def homepage_user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)

        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user to the page for administrating his library and for viewing the libraries of his friends.
                login(request, user)
                return HttpResponseRedirect('/lista_carti_galerie/')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Contul dvs. Bookeeper este dezactivat.")
        else:
            # Bad login details were provided. So we can't log the user in.
            # Redirect back to the same pag
            bad_login_msg = "Datele dvs. de autentificate sunt gresite. Asa ca nu va putem lasa sa intrati in biblioteca."
            return render(request, 'index.html', context={'form': form, 'bad_login_data': bad_login_msg})

    # If a GET (or any other method) we will create a blank form.
    else:
        form = LoginForm()
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render(request, 'index.html', {'form': form})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))

################### View for displaying all books for a logged in user, with all administration rights: add a book, delete / modify it   ##############################
################### The logged in user can also see the books of the users that are in his friends' list and send them requests if he wants to borrow a book from them    ##############

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!   TO ADD FUNCTIONALITY FOR DISPLAYING BOOKS FROM THE LIBRARY OF OTHER USERS (FRIENDS / WHICH ARE NOT LOGGED IN)


def get_user_type(request):
    #### Test user type and return it's id

    current_user = request.user

    if request.user.is_authenticated:
        active_user = Profile.objects.get(user=request.user)
        return active_user
    else:
        #########  !!!!!!!!!!!!!! TO BE IMPLEMENTED #################################
        ### to obtain the id of the other user (who is clicked on after login in)
        return "user_without_access"


def attach_menu_data(request):

    #### if the user is authenticated (logged in), than obtain his id, in order to display the room from his own library
    #### (each active user / logged in, can have it's library with administration of it's own rooms and shelfs)
    ### for the other users / not logged in, the rooms will be an empty list (they can't have access for viewing the books from a certain selected room of the active (logged in) users)

    user_type = get_user_type(request)

    if user_type != "user_without_access":
        user_no_access = False
        active_user = user_type
        active_user_full_name = user_type.user.first_name + " " + user_type.user.last_name
        rooms = Room.objects.filter(owner=user_type.id).order_by('name')
        if rooms:
            library_rooms = rooms
        else:
            library_rooms = []
    else:
        user_no_access = True

    main_categories = Category.objects.filter(parent_category=None).order_by('name')
    subcategories = Category.objects.filter(parent_category__isnull=False).order_by('name')

    context_categories = []

    for category in main_categories:
        catogryDict = {"main_category": category}
        catogryDict['subcategories'] = []
        for subcat in subcategories:
            if subcat.parent_category.id == category.id:
                catogryDict['subcategories'].append(subcat)
        context_categories.append(catogryDict)

    context = {
        'active_user_rooms': library_rooms,
        'active_user_name':active_user_full_name,
        'active_user_id':active_user.id,
        'inactive_user':user_no_access,
        'categ_subcateg':context_categories
    }

    return context

def associate_book_all_authors_name(books_from_DB):

    ### for each book from DB (received as parameter), create a dictionary with the book and a list with it's author or authors (in case there are multiple authors for the same book)
    ### example: a book can have multiple authors which contribute to eatch other - for example, a scientific book

    book_authors = []

    for book in books_from_DB:
        dictBookAuthors = {}
        dictBookAuthors['the_book'] = book
        authors_list = []
        authors = book.authors.values()
        for author in authors:
            author_fullname = author['firstname'] + " " + author['lastname']
            authors_list.append(author_fullname)

        author_names= ", "
        dictBookAuthors['all_authors_names'] = author_names.join(authors_list)   ### create a string/text, with all authors's of a book (their full names), extracted from the list created earlier
        book_authors.append(dictBookAuthors)

    return book_authors


def display_all_user_books(request):

    context = attach_menu_data(request)

    if context['inactive_user']==False:

        ### the user is active (logged in) => obtain the list of all it's books
        all_books_active_user = Book.objects.all().filter(owner_id=context['active_user_id'])

        context['books_owner'] = all_books_active_user
        context['books_authors'] = associate_book_all_authors_name(all_books_active_user)

        return render(request, 'lista_carti_galerie.html', context)
    else:
        ###################### TO BE IMPLEMENTED FOT THE OTHER USERS / NOT ACTIVE / NOT LOGGED IN (usual users or group of friends)  ##############
        pass



def display_DB_rooms_book_categ_subcateg(request):
    context = attach_menu_data(request)

    # return render(request, 'base_gallery.html', context)
    return render(request, 'base_gallery.html', context)


def books_room_detail(request,pk):

    context = attach_menu_data(request)

    #only for the logged in users (active)
    room = Room.objects.get(id=pk, owner_id=context['active_user_id'])
    shelfs = Shelf.objects.filter(room_id=room.id)

    if shelfs:
        rooms_shelfs = shelfs
        books_room = Book.objects.filter(shelf__in=shelfs)
    else:
        rooms_shelfs = []
        books_room = []

    # attach the context for the top nagivation bar, and add needed values to it

    context['room'] = room
    context['shelfs'] = rooms_shelfs
    context['books_from_room'] = books_room
    context['books_authors'] = associate_book_all_authors_name(books_room)

    return render(request,'lista_carti_camera.html',context)


def books_by_categ_subcateg(request,pk):

    # attach the context for the top nagivation bar and user welcome text
    context = attach_menu_data(request)

    if context['active_user_id']:
        ### if the user is active (logged in)

        #obtain the id of the user, depending on his type: an user who sent the request (the logged in one/active) or the current user (ex: friend)
        user_type_id = context['active_user_id']

    else:
        ###########  TO BE IMPLEMENTED -> TO OBTAIN THE USER'S ID FOR THE OTHER USERS (INACTIVE / FRIENDS) ##############
        # user_type_id =   ................
        pass

    # obtain the list of books, by category / subcategory, depending on the type of user (active/inactive)
    books_by_categ = Book.objects.all().filter(owner_id=user_type_id).filter(category=pk)

    context['books_categ_subcateg'] = books_by_categ
    context['books_authors'] = associate_book_all_authors_name(books_by_categ)

    return render(request, 'lista_carti_categ_subcateg.html', context)


def book_detailed_info(request,pk):

    user_type = get_user_type(request)

    if user_type != "user_without_access":
        #### only for the active users (logged in), suplimentary information will be displayed (like room and shelf, in order to easily find the localion of the book in the house)
        user_no_access = False
    else:
        user_no_access = True

    context = {
        'inactive_user':user_no_access,
    }

    book_detailed_info = Book.objects.get(owner=user_type.id,id=pk)

    context['book_details'] = book_detailed_info

    if book_detailed_info.shelf.shelf_number==0:
        library_without_shelfs_msg = "(camera nu are biblioteca impartita in rafturi verticale / doar carti in dulapuri, pe diferite nivele, sau cartea nu are un loc fix in camera)"
    else:
        library_without_shelfs_msg = False

    context['no_shelfs_msg'] = library_without_shelfs_msg

    #find all authors of the book (in case there are multiple authors for the same book)
    authors_list = []
    authors = book_detailed_info.authors.values()
    for author in authors:
        author_fullname = author['firstname'] + " " + author['lastname']
        authors_list.append(author_fullname)

    author_names = ", "
    context['all_authors_names'] = author_names.join(authors_list)


    # find the name of the book's category / and, in case it has subcategories, also the name of the subcategory
    categ_id = book_detailed_info.category_id
    if categ_id:
        if Category.objects.get(id=categ_id).parent_category:
            #### the book is in a subcategory, find the names of it's subcategory and category
            subcateg_name = Category.objects.get(id=categ_id).name
            categ_name= Category.objects.get(id=book_detailed_info.category.parent_category.id).name
        else:
            categ_name = Category.objects.get(id=categ_id).name
            subcateg_name = False
    else:
        # the book isn't registered in any category
        categ_name = False

    context['categ_name'] = categ_name
    context['subcateg_name'] = subcateg_name

    return render(request, 'detalii_carte.html',context)


class BookUpdateView(UpdateView):
 model = Book
 template_name = 'modifica_info_carte.html'
 form_class = BookUpdateForm

 success_url = reverse_lazy('galerie_carti')



# View for processing the data entered by user in the form for adding a book in the library

def add_books(request):

    active_user = Profile.objects.get(user=request.user)

    logged_username = active_user.user.username

    authors = Author.objects.all().order_by('firstname','lastname')
    authors_id_names = []
    for author in authors:
        # for each author, create a dictionary with it's id and full name
        authorDict = {}
        authorDict['author_id'] = author.id
        author_fullname = author.firstname + " " + author.lastname
        authorDict['author_name'] = author_fullname

        authors_id_names.append(authorDict)

    publishers = Publisher.objects.all().order_by('publisherName')

    context = attach_menu_data(request)


    rooms = Room.objects.filter(owner=active_user.id).order_by('name')

    ### find the active user shelfs (from his rooms)
    rooms_ids = []
    if rooms:
        for room in rooms:
            rooms_ids.append(room.id)


    shelfs = Shelf.objects.filter(room_id__in=rooms_ids)

    main_categories = Category.objects.filter(parent_category=None).order_by('name')
    subcategories = Category.objects.filter(parent_category__isnull=False).order_by('name')

    ### ------ create a list of dictionary: keys = 1) room; 2) for each room, associate the shelfs from it
    # rooms_shelfs = []
    # if rooms:
    #        for room in rooms:
    #            room_shelfs = {}
    #            room_shelfs['room'] = room
    #            shelfs_in_room = Shelf.objects.filter(room_id=room.id)
    #            if shelfs_in_room:
    #                room_shelfs['shelfs'] = shelfs_in_room
    #            else:
    #                room_shelfs['shelfs'] = False
    #            rooms_shelfs.append(room_shelfs)
    #
    # else:
    #     rooms_shelfs = False



    context['username'] = logged_username
    context['all_authors'] = authors_id_names
    context['db_publishers'] = publishers
    context['user_rooms'] = rooms
    context['user_shelfs'] = shelfs
    context['book_categories'] = main_categories
    context['categ_subcategories'] = subcategories
    # context['room_with_shelfs'] = rooms_shelfs
    # context['shelfs'] = rooms_shelfs

    #### ---------------- add validation for the form fields / processing of data ----------------------
    if request.method=='POST':
        form = AddBookForm(request.POST)

        if form.is_valid():
            return redirect('galerie_carti')
        else:
            #return back to the same page if the date was invalid
            context['form'] = form
            return render(request, 'adauga_carti.html', context)

    else:
        form = AddBookForm()
        return render(request, 'adauga_carti.html', context)





