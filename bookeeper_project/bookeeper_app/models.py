from django.db import models
from django.contrib.auth.models import User
from django.db.models import IntegerField
from accounts.models import Profile

#### imports for special field ot the Book model
from isbn_field import ISBNField

#########################   Added a field to the old Category model: parent_category (Foreign key to the same model Category/self)
# ############################   because a Category can have Subcategories or not ###################
######### parent_category is the id of the main category (if it is Null, than it has no subcategories)
########## if parent_category has a value, than it means that we've created a subcategory with the id of the main category

class Category(models.Model):
    name = models.CharField(max_length=40)
    parent_category = models.ForeignKey("self", on_delete=models.DO_NOTHING,null=True, blank=True)

    def __str__(self):
         return self.name

class Room(models.Model):
    name = models.CharField(max_length=40)
    owner = models.ForeignKey(Profile, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name


class Shelf(models.Model):
    room = models.ForeignKey(Room, on_delete=models.DO_NOTHING)
    shelf_number = models.IntegerField()

    def __str__(self):
        return f"user_id: {self.room.owner}: {self.room.name} - Raftul - {self.shelf_number}"


# ############################ ------ Added 2 new tables / models for association with the Book Model ------- #############################
# ############# An author can have multiple book #############################
# ############# A publisher can write multiple books (we are not interested in our case, if a book can also be written/edited by multiple publishers and which are those)  #############
#
class Author(models.Model):
    firstname = models.CharField(max_length=30,null=False)
    lastname = models.CharField(max_length=30,null=False)

    def __str__(self):
        return f"{self.firstname} {self.lastname}"


class Publisher(models.Model):
    publisherName = models.CharField(max_length=50,null=False, unique=True)

    def __str__(self):
        return self.publisherName


class Book(models.Model):
    title = models.CharField(max_length=70,null=False)
    description = models.TextField()

    category = models.ForeignKey(
        Category,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    owner = models.ForeignKey(
        Profile,
        on_delete=models.DO_NOTHING,
        null=False
    )

    shelf = models.ForeignKey(
        Shelf,
        on_delete=models.DO_NOTHING
    )

    ### An author can write many books and a book can have multiple authors (who collaborate to eatch other for the same book)
    authors = models.ManyToManyField(
        "Author",
        null=False
    )

    ### Old structure of a book contained only an author for a book, not having the possibility to select multiple authors for the same book (a book at which contribute more authors)
    # author = models.ForeignKey(
    #         Author,
    #         on_delete=models.DO_NOTHING,
    #         null=False
    #     )


    isbn = ISBNField()

###### Issues at server run: 'max_length' is ignored when used with IntegerField.  HINT: Remove 'max_length' from field -> removed max_lenth, the validation will be done in views

    # copyright_year = IntegerField(null=True,blank=True,max_length=4)
    # edition_year = IntegerField(null=True,blank=True,max_length=4)
    copyright_year = IntegerField(null=True, blank=True)
    edition_year = IntegerField(null=True,blank=True)

    publisher = models.ForeignKey(
        Publisher,
        on_delete=models.DO_NOTHING,
        null=True
    )

    cover_image = models.ImageField(null=False,upload_to='coperti_carti/')

    def __str__(self):
        return self.title
