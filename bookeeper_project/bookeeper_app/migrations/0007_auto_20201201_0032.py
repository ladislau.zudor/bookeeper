# Generated by Django 3.1.3 on 2020-11-30 22:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookeeper_app', '0006_auto_20201129_1457'),
    ]

    operations = [
        migrations.RenameField(
            model_name='book',
            old_name='authorID',
            new_name='author',
        ),
        migrations.RenameField(
            model_name='book',
            old_name='publisherID',
            new_name='publisher',
        ),
    ]
