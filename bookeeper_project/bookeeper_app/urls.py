from django.conf import settings
from django.urls import path
from . import views
from django.contrib.staticfiles.urls import static

from .views import BookUpdateView

urlpatterns = [
    ##########   this is for not having to use / at the end of each url  #################
    #url(r'^books_gallery/$', views.BooksPageView.as_view(), name='books_gallery'),

    path('', views.homepage_user_login, name='home'),
    path('base_gallery/', views.display_DB_rooms_book_categ_subcateg, name="rooms_categories_menu"),

    path('lista_carti_galerie/', views.display_all_user_books, name="galerie_carti"),
    path('lista_carti_camera/<int:pk>', views.books_room_detail, name="carti_camera"),
    path('lista_carti_categ_subcateg/<int:pk>', views.books_by_categ_subcateg, name="carti_pe_categorii"),

    path('detalii_carte/<int:pk>', views.book_detailed_info, name="detalii_info_carte"),

    path('modifica_carte/<int:pk>', BookUpdateView.as_view(), name="modifica_info_carte"),
    path('adauga_carti/', views.add_books, name="adaugare_carti"),

    path('logout/', views.user_logout, name='logout'),
]

#####################      Settings for uploading book images in the media directory (created automatically at the first upload)     #########################
if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)

