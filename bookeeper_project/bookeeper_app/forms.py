# forms.py
from django import forms
from .models import *


class LoginForm(forms.Form):
    username = forms.CharField(label='Utilizator/Nume cont', max_length=30)
    password = forms.CharField(label='Parola', max_length=20, widget=forms.PasswordInput())


# # -----   Create the form automatically for modifying book information   ------------
class BookUpdateForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = ['title', 'authors', 'description', 'category', 'shelf', 'isbn','edition_year','copyright_year', 'publisher', 'cover_image']

        labels = {
            "title": "Titlul cartii",
            "authors":"Autori",
            "description":"Descriere",
            "category":"Categoria",
            "shelf":"Raftul",
            "edition_year":"Anul aparitiei",
            "copyright_year":"Anul editarii",
            "publisher":"Editura",
            "cover_image":"Poza coperta carte",
        }

    #filter fields values, only for the active user / logged in

    # def __init__(self, user, *args, **kwargs):
    #     super(BookUpdateForm, self).__init__(*args, **kwargs)
    #     active_user = Profile.objects.get(user=self.request.user)
    #     logged_in_user_books = Book.objects.filter(owner=active_user)
    #     self.fields['shelf'].queryset = Book.objects.filter(shelf=logged_in_user_books.shelf)



class AddBookForm(forms.Form):
    book_title = forms.CharField(max_length=70)
    # selected_authors=forms.ChoiceField(choices=????)    #### ???? cum sa fac legatura cu valorile din option din form, la select simplu si multiplu
    # publisher=forms.ChoiceField(choices=????)
    # selected_rooms = forms.ChoiceField(choices=????)
    # selected_shelfs = forms.ChoiceField(choices=????)
    # selected_categories = forms.ChoiceField(choices=????)
    # selected_subcategories = forms.ChoiceField(choices=????)

    book_description = forms.CharField(required=False, widget=forms.Textarea(attrs={"class":"form-control","id":"book_descr","rows":7,"cols":50 }))
    isbn = forms.CharField(max_length=20)
    edition_year = forms.IntegerField()
    copyright_year = forms.IntegerField()
    cover_image = forms.ImageField()

    def clean(self):
        super(AddBookForm,self).clean()

        book_title = self.cleaned_data.get('book_title','')


        #extract data entered in form's fields

        #form validation
        if len(book_title) > 70:
            # self._errors['book_title'] = self.error_class(['Maxim 70 caractere.'])
            raise forms.ValidationError('Titlul trebuie sa contina maxim 70 caractere')

        #returns any errors if found
        return self.cleaned_data






