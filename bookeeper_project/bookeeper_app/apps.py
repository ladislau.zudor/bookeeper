from django.apps import AppConfig


class BookeeperAppConfig(AppConfig):
    name = 'bookeeper_app'
