from django.contrib.auth.models import User
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save


# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    friends = models.ManyToManyField("Profile", blank=True)

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return f"/users/{self.pk}"


def post_save_user_model_receiver(sender, instance, created, *args, **kwargs):
    if created:
        try:
            Profile.objects.create(user=instance)
        except:
            pass


post_save.connect(post_save_user_model_receiver, sender=settings.AUTH_USER_MODEL)


class FriendRequest(models.Model):
    to_profile = models.ForeignKey(Profile, related_name='to_user', on_delete=models.DO_NOTHING)
    from_profile = models.ForeignKey(Profile, related_name='from_user', on_delete=models.DO_NOTHING)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"From {self.from_profile.user.username}, to {self.to_profile.user.username}."
