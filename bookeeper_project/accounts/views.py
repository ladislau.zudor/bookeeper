from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from .models import Profile, FriendRequest


# User = get_user_model()

def active_user(request):
    current_user = request.user.id
    return HttpResponseRedirect(f'/users/{current_user}')

def users_list(request):
    users = Profile.objects.exclude(user=request.user)
    context = {
        'users': users
    }
    return render(request, "accounts/home.html", context)


def send_friend_request(request, id):
    if request.user is not None and not request.user.is_anonymous:
        current_profile = Profile.objects.get(user=request.user)
        active_profile = Profile.objects.get(user_id=id)
        frequest, created = FriendRequest.objects.get_or_create(
            from_profile=current_profile,
            to_profile = active_profile,
        )
        return HttpResponseRedirect('/users')


def cancel_friend_request(request, id):
    if request.user is not None and not request.user.is_anonymous:
        current_profile = Profile.objects.get(user=request.user)
        active_profile = Profile.objects.get(user_id=id)
        frequest = FriendRequest.objects.filter(
            from_profile=current_profile,
            to_profile=active_profile,
        ).first()
        frequest.delete()
        return HttpResponseRedirect('/users')


def accept_friend_request(request, id):
    current_profile = Profile.objects.get(user=request.user)
    active_profile = Profile.objects.get(user_id=id)
    frequest = FriendRequest.objects.get(from_profile=active_profile, to_profile=current_profile)
    current_profile.friends.add(active_profile)
    active_profile.friends.add(current_profile)
    frequest.delete()
    return HttpResponseRedirect('/users/{}'.format(request.user.pk))


def delete_friend_request(request, id):
    current_profile = Profile.objects.get(user=request.user)
    active_profile = Profile.objects.get(user_id=id)
    frequest = FriendRequest.objects.get(from_profile=active_profile, to_profile=current_profile)
    frequest.delete()
    return HttpResponseRedirect('/users/{}'.format(request.user.pk))


def profile_view(request, pk):
    current_profile = Profile.objects.get(id=pk)
    active_profile = Profile.objects.get(user=request.user)

    u = current_profile.user
    sent_friend_requests = FriendRequest.objects.filter(from_profile=current_profile)
    rec_friend_requests = FriendRequest.objects.filter(to_profile=current_profile)

    friends = current_profile.friends.all()

    # is this user our friend
    button_status = 'none'
    is_friend = current_profile in active_profile.friends.all()

    if not is_friend:
        button_status = 'not_friend'

        # if we have sent him a friend request
        if len(FriendRequest.objects.filter(
                from_profile=active_profile).filter(to_profile=current_profile)) == 1:
            button_status = 'friend_request_sent'

    users = Profile.objects.exclude(user=request.user)

    context = {
        'current_profile': current_profile,
        'u': u,
        'button_status': button_status,
        'friends_list': friends,
        'sent_friend_requests': sent_friend_requests,
        'rec_friend_requests': rec_friend_requests,
        'users': users,
    }

    return render(request, "accounts/profile.html", context)
