from django.conf.urls import url
from django.urls import path

from .views import (
    users_list,
    profile_view,
    send_friend_request,
    cancel_friend_request,
    accept_friend_request,
    delete_friend_request,
    active_user,
)

urlpatterns = [
    path('', users_list, name='list'),
    path('<int:pk>/', profile_view),
    path('active-user/', active_user, name='active_user'),
    path('friend-request/delete/<int:id>/', delete_friend_request),
    path('friend-request/cancel/<int:id>/', cancel_friend_request),
    path('friend-request/accept/<int:id>/', accept_friend_request),
    path('friend-request/send/<int:id>/', send_friend_request),
]
